# Summary
This library provides abstraction layer for Java-powered extensions to be embedded into JNI-compatible runtimes.
It is a part of the project to allow Java coding within game projects based on high-end game engines.


# Solution architecture
1.  Host loads JVM as dynamic library. 
1.  Host starts JVM at the same process.
1.  Host invokes JVM lifecycle methods (start/update/lateUpdate/stop) at right time using JNI.
1.  Host writes data to state monitor buffer at the off-jvm-heap space. Format of state monitor buffer 
is plain bytes with named markers.
1.  JVM reads state monitor buffer.
1.  JVM invokes host method using JNI to allocate as many command queues as needed.
1.  JVM writes to command queue buffer at the off-jvm-heap space (using NIO direct buffers or `sun.misc.Unsafe`). 
Format of commands is plain bytes.
1.  JVM invokes special host method using JNI to signalize that command buffer is ready to be executed.
1.  Host executes commands.
1.  JVM and Host create any suitable high-level abstractions over command queue and state monitor buffers. It would 
be any 3rd party or hand-made marshalling technology.
