package com.nplekhanov.riseofj.nestedjvm.api4host;

/**
 * there is no magic - just marker.
 */
public @interface UsedFromHostRuntime {
}
