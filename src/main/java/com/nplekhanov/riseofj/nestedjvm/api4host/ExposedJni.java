package com.nplekhanov.riseofj.nestedjvm.api4host;

import com.nplekhanov.riseofj.nestedjvm.api.HostExtension;
import com.nplekhanov.riseofj.nestedjvm.bootstrap.ExtensionInitializer;
import org.jetbrains.annotations.NotNull;

final class ExposedJni {

    private ExposedJni() {
    }

    @UsedFromHostRuntime
    private static @NotNull HostExtension createHostExtension(
        final @NotNull String extensionFactoryName
    ) throws Exception {
        return ExtensionInitializer.createHostExtension(extensionFactoryName);
    }

}
