package com.nplekhanov.riseofj.nestedjvm.api4host;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;

public final class ProvidedJni {

    private ProvidedJni() {
    }

    public static native long allocateCommandQueue(@NotNull String queueName, int bufferSize);

    public static native @NotNull ByteBuffer resolveCommandQueueBuffer(long queueId);

    public static native void submitCommandQueue(long queueId);

    public static native @Nullable ByteBuffer lookupMonitoringWindowBuffer(final @NotNull String variableName);
}
