package com.nplekhanov.riseofj.nestedjvm.api;

import com.nplekhanov.riseofj.nestedjvm.api4host.UsedFromHostRuntime;

public interface HostExtension {

    @UsedFromHostRuntime
    void start();

    @UsedFromHostRuntime
    void update();

    @UsedFromHostRuntime
    void lateUpdate();

    @UsedFromHostRuntime
    void stop();
}