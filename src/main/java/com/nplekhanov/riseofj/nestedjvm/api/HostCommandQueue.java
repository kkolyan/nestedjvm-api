package com.nplekhanov.riseofj.nestedjvm.api;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

public interface HostCommandQueue {

    @NotNull String getQueueName();

    @NotNull ByteBuffer getOutgoingBuffer();

    void submit();
}
