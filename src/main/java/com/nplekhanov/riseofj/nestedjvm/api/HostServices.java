package com.nplekhanov.riseofj.nestedjvm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HostServices {

    @Nullable HostMonitoringWindow getMonitoringWindow(@NotNull String windowName);

    @NotNull HostCommandQueue createCommandQueue(@NotNull String queueName, final int bufferSize);
}
