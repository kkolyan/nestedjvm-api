package com.nplekhanov.riseofj.nestedjvm.api;

import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;

public interface HostMonitoringWindow {

    @Nullable ByteBuffer getIncomingBuffer();

    String getWindowName();
}
