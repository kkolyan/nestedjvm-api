package com.nplekhanov.riseofj.nestedjvm.api;

import org.jetbrains.annotations.NotNull;

public interface HostExtensionFactory {
    @NotNull HostExtension createExtension(@NotNull HostServices hostServices);
}
