package com.nplekhanov.riseofj.nestedjvm.bootstrap;

import com.nplekhanov.riseofj.nestedjvm.api.HostExtension;
import com.nplekhanov.riseofj.nestedjvm.api.HostExtensionFactory;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;

public final class ExtensionInitializer {
    private ExtensionInitializer() {
    }

    @NotNull
    public static HostExtension createHostExtension(
        final @NotNull String extensionFactoryName
    ) throws
        ClassNotFoundException,
        InstantiationException,
        IllegalAccessException,
        InvocationTargetException,
        NoSuchMethodException
    // preserve line break
    {
        final Class<?> extensionFactoryClass = Class.forName(extensionFactoryName);
        final HostExtensionFactory extensionFactory = (HostExtensionFactory) extensionFactoryClass
            .getDeclaredConstructor()
            .newInstance();
        return extensionFactory.createExtension(new NativeHostServices());
    }
}
