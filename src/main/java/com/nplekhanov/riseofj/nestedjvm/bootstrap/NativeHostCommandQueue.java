package com.nplekhanov.riseofj.nestedjvm.bootstrap;

import com.nplekhanov.riseofj.nestedjvm.api4host.ProvidedJni;
import com.nplekhanov.riseofj.nestedjvm.api.HostCommandQueue;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

final class NativeHostCommandQueue implements HostCommandQueue {
    private final long queueId;
    private final ByteBuffer buffer;
    private final String queueName;

    public NativeHostCommandQueue(final @NotNull String queueName, final int bufferSize) {
        this.queueName = queueName;
        queueId = ProvidedJni.allocateCommandQueue(queueName, bufferSize);
        buffer = ProvidedJni.resolveCommandQueueBuffer(queueId);
    }

    @Override
    public void submit() {
        ProvidedJni.submitCommandQueue(queueId);
    }

    @Override
    public @NotNull ByteBuffer getOutgoingBuffer() {
        return buffer;
    }

    @Override
    public @NotNull String getQueueName() {
        return queueName;
    }
}
