package com.nplekhanov.riseofj.nestedjvm.bootstrap;

import com.nplekhanov.riseofj.nestedjvm.api.HostCommandQueue;
import com.nplekhanov.riseofj.nestedjvm.api.HostServices;
import com.nplekhanov.riseofj.nestedjvm.api.HostMonitoringWindow;
import org.jetbrains.annotations.NotNull;

final class NativeHostServices implements HostServices {

    @Override
    public @NotNull HostMonitoringWindow getMonitoringWindow(@NotNull final String windowName) {
        return new NativeHostMonitoringWindow(windowName);
    }

    @Override
    public @NotNull HostCommandQueue createCommandQueue(final @NotNull String queueName, final int bufferSize) {
        return new NativeHostCommandQueue(queueName, bufferSize);
    }
}
