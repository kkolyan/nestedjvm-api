package com.nplekhanov.riseofj.nestedjvm.bootstrap;

import com.nplekhanov.riseofj.nestedjvm.api.HostMonitoringWindow;
import com.nplekhanov.riseofj.nestedjvm.api4host.ProvidedJni;

import java.nio.ByteBuffer;

public final class NativeHostMonitoringWindow implements HostMonitoringWindow {
    private final String windowName;
    private ByteBuffer incomingBuffer;

    public NativeHostMonitoringWindow(final String windowName) {
        this.windowName = windowName;
    }

    @Override
    public ByteBuffer getIncomingBuffer() {
        if (incomingBuffer == null) {
            incomingBuffer = ProvidedJni.lookupMonitoringWindowBuffer(windowName);
        }
        return incomingBuffer;
    }

    @Override
    public String getWindowName() {
        return windowName;
    }
}
